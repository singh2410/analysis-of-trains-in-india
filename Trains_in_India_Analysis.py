#!/usr/bin/env python
# coding: utf-8

# # Trains in India Analysis
# #By- Aarush Kumar
# #Dated: July 22,2021

# In[1]:


from IPython.display import Image
Image(url='https://akm-img-a-in.tosshub.com/businesstoday/images/story/201903/vande-bharat-660_030719125448.jpg')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import folium
from folium.map import Icon


# In[3]:


df_trains = pd.read_csv('/home/aarush100616/Downloads/Projects/Trains in India EDA/All_Indian_Trains.csv')
df_cities = pd.read_csv("/home/aarush100616/Downloads/Projects/Trains in India EDA/cities_r2.csv")
df_wcities = pd.read_csv("/home/aarush100616/Downloads/Projects/Trains in India EDA/worldcitiespop.csv")


# In[4]:


df_trains


# In[6]:


df_cities


# In[7]:


df_wcities


# In[8]:


sub_wcities = pd.concat([df_wcities[df_wcities['Country'] == 'in'], df_wcities[df_wcities['Country'] == 'bd'] ,df_wcities[df_wcities['Country'] == 'bt'],df_wcities[df_wcities['Country'] == 'np'],df_wcities[df_wcities['Country'] == 'pk']])


# In[9]:


df_trains.head(10)


# In[10]:


len(df_trains)


# In[11]:


sub_wcities.head(10)


# In[12]:


len(sub_wcities[sub_wcities['Country'] == 'in'])


# In[13]:


df_cities.head(10)


# ## Data cleaning

# In[14]:


def corr(name):
    if name == 'Velankanni' or name == 'Vellankanni':
        return 'Velanganni'
    elif name == 'Raxual Junction':
        return 'Raxaul Junction'
    elif name == 'Alipur Duar Junction':
        return 'Alipurduar Junction'
    elif name == 'Chamarajanagar':
        return 'Chamarajnagar'
    elif name == 'Dehradun':
        return 'Dehra Dun'
    elif name == 'Eranakulam Junction':
        return 'Ernakulam Junction'
    elif name == 'Machelipatnam':
        return 'Machilipatnam'
    elif name == 'Metupalaiyam':
        return 'Mettupalaiyam'
    elif name == 'Mathura Junction':
        return 'Vrindavan'               
    elif name == 'Murkeongselek':
        return 'Murkong Selek'
    elif name == 'Nagarsol':
        return 'Nagarsul'
    elif name == 'New Delhi':
        return 'Newdelhi'
    elif name == 'Tiruchchirapali':
        return 'Tiruchchirappalli'
    elif name == 'Villuparam Junction':
        return 'Villupuram Junction'
    elif name == 'Vishakapatnam':
        return 'Vishakhapatnam'
    else:
        return name


# ### Map of the stations

# In[15]:


ds = df_trains['Starts'].apply(corr)
de = df_trains['Ends'].apply(corr)
df_trains_aug = pd.DataFrame()      
df_trains_aug['Train no.'] = df_trains['Train no.']
df_trains_aug['Train name'] = df_trains['Train name']
df_trains_aug['Starts'] = ds
df_trains_aug['Ends'] = de
df_trains_aug.head(10)


# In[16]:


df_stations = pd.DataFrame()       # Will group info about all the stations
sta_name = []
sta_city = []
sta_lat = []
sta_long = []
sta_starts = []
sta_ends = []
sta_trains = []
sta_state = []
sta_country = []
unfound = []
stations_set = set(df_trains_aug['Starts']).union(set(df_trains_aug['Ends']))


# In[17]:


for s in stations_set:
    found = False
    for w in sub_wcities['City']:
        if not found:
            if s.lower() in str(w).split(' ') or str(w) in s.lower().split(' ') or str(w) == s.lower():
                sta_name.append(s)
                sta_city.append(str(w))
                sta_lat.append(sub_wcities[sub_wcities['City'] == str(w)]['Latitude'].to_numpy()[0])
                sta_long.append(sub_wcities[sub_wcities['City'] == str(w)]['Longitude'].to_numpy()[0])
                sta_starts.append(len(df_trains_aug[df_trains_aug['Starts'] == s]))
                sta_ends.append(len(df_trains_aug[df_trains_aug['Ends'] == s]))
                sta_trains.append(len(df_trains_aug[df_trains_aug['Starts'] == s]) + len(df_trains_aug[df_trains_aug['Ends'] == s]))
                sta_state.append(sub_wcities[sub_wcities['City'] == str(w)]['Region'].to_numpy()[0])
                sta_country.append(sub_wcities[sub_wcities['City'] == str(w)]['Country'].to_numpy()[0])
                found = True
    if not found:
        unfound.append(s)


# In[18]:


sta_starts[:10]
sta_ends[:10]


# In[19]:


len(unfound)


# In[20]:


unfound


# In[21]:


manual_handle = {'Chirmiri':'korea', 'Manduadih':'varanasi', 'Sadulpur Junction':'churu', 'Manuguru':'kothagudem', 'Mayiladuturai J':'mayuram', 'Sengottai':'tenkasi',
                'Kochuveli':'thiruvananthapuram', 'Patliputra':'danapur', 'Chamarajnagar':'mysore', 'C Shahumharaj T':'kolhapur', 'Lokmanyatilak T':'kurla', 'Gevra Road':'korba',
                'Singrauli':'churki', 'Shmata V D Ktra':'dudura', 'New Alipurdaur':'alipur duar', 'Alipurduar Junction':'alipur duar', 'Habibganj':'bhopal', 'Banaswadi':'bangalore', 'Jhajha':'jamui',
                'Sawantwadi Road':'talavada', 'H Nizamuddin':'delhi', 'Naharlagun':'itanagar', 'Nilaje':'mumbai', 'Khairthal':'alwar', 'Udhna Junction':'surat', 'Kirandul':'dantewara',
                'Kacheguda':'hyderabad', 'Belampalli':'mancherial', 'Radhikapur':'raiganj', 'Borivali':'mumbai', 'Dekargaon':'tezpur', 'Newdelhi': 'new delhi'}
for s in manual_handle.keys():
    sta_name.append(s)
    sta_city.append(manual_handle[s])
    sta_lat.append(sub_wcities[sub_wcities['City'] == manual_handle[s]]['Latitude'].to_numpy()[0])
    sta_long.append(sub_wcities[sub_wcities['City'] == manual_handle[s]]['Longitude'].to_numpy()[0])
    sta_starts.append(len(df_trains_aug[df_trains_aug['Starts'] == s]))
    sta_ends.append(len(df_trains_aug[df_trains_aug['Ends'] == s]))
    sta_trains.append(len(df_trains_aug[df_trains_aug['Starts'] == s]) + len(df_trains_aug[df_trains_aug['Ends'] == s]))
    sta_state.append(sub_wcities[sub_wcities['City'] == manual_handle[s]]['Region'].to_numpy()[0])
    sta_country.append(sub_wcities[sub_wcities['City'] == manual_handle[s]]['Country'].to_numpy()[0])


# In[22]:


df_stations['name'] = sta_name
df_stations['city'] = sta_city
df_stations['latitude'] = sta_lat
df_stations['longitude'] = sta_long
df_stations['nb_starts'] = sta_starts
df_stations['nb_ends'] = sta_ends
df_stations['nb_trains'] = sta_trains
df_stations['state'] = sta_state
df_stations['country'] = sta_country


# In[23]:


df_stations.describe()


# In[24]:


stations_map = folium.Map(location=[22.05, 78.94], zoom_start=4.5)
for idx, row in df_stations.iterrows():
    c = 'mediumpurple'
    if row['nb_ends'] == 0:
        c = 'royalblue'
    if row['nb_starts'] == 0:
        c = 'deeppink'
    folium.Circle(location=[row['latitude'], row['longitude']], radius=1 + 400 * row['nb_trains'], color = c, fill = True, popup = row['name']).add_to(stations_map)
stations_map


# This map shows all the stations of the dataset. Circles radius depend on the number of train of the station. Stations in blue are start stations, stations in red are end stations, and purple stations can be start or end. We can see that blue and red stations are only small stations. Stations in foreing countries are small, because we show only their trains for India. We can guess that are many internal trains as well. 

# In[25]:


df_stations.sort_values('nb_trains',ascending=False).head(10)


# We see that there are four stations with more than 200 trains. Howrah Junction is the first one in number of trains, as well as in number of starting and ending trains. So, let's draw the map of Howrah Junction's trains !

# In[26]:


howrah_lines = folium.Map(location=[22.59,88.31], zoom_start=4.5)
x0 = df_stations[df_stations['name'] == 'Howrah Junction']['latitude'].to_numpy()[0]
x1 = df_stations[df_stations['name'] == 'Howrah Junction']['longitude'].to_numpy()[0]
folium.Marker(location=(x0, x1), icon=Icon(color='purple', icon='train')).add_to(howrah_lines)
for idx, row in df_trains_aug.iterrows():
    if row['Starts'] == 'Howrah Junction':
        y0 = df_stations[df_stations['name'] == [row['Ends']][0]]['latitude'].to_numpy()[0]
        y1 = df_stations[df_stations['name'] == [row['Ends']][0]]['longitude'].to_numpy()[0]
        folium.Marker(location=(y0, y1), icon=Icon(color='green', icon='train')).add_to(howrah_lines)
    elif row['Ends'] == 'Howrah Junction':
        y0 = df_stations[df_stations['name'] == [row['Starts']][0]]['latitude'].to_numpy()[0]
        y1 = df_stations[df_stations['name'] == [row['Starts']][0]]['longitude'].to_numpy()[0]
        folium.Marker(location=(y0, y1), icon=Icon(color='orange', icon='train')).add_to(howrah_lines)
howrah_lines


# ### let's look at the international lines

# In[27]:


foreign_lines = folium.Map(location=[22.05, 78.94], zoom_start=4.5)
for idx, row in df_trains_aug.iterrows():
    if df_stations[df_stations['name'] == row['Starts']]['country'].to_numpy()[0] != 'in':
        x0 = df_stations[df_stations['name'] == [row['Starts']][0]]['latitude'].to_numpy()[0]
        x1 = df_stations[df_stations['name'] == [row['Starts']][0]]['longitude'].to_numpy()[0]
        y0 = df_stations[df_stations['name'] == [row['Ends']][0]]['latitude'].to_numpy()[0]
        y1 = df_stations[df_stations['name'] == [row['Ends']][0]]['longitude'].to_numpy()[0]
        folium.PolyLine(locations=[(x0, x1),(y0, y1)], color='limegreen').add_to(foreign_lines)
    elif df_stations[df_stations['name'] == row['Ends']]['country'].to_numpy()[0] != 'in':
        x0 = df_stations[df_stations['name'] == [row['Starts']][0]]['latitude'].to_numpy()[0]
        x1 = df_stations[df_stations['name'] == [row['Starts']][0]]['longitude'].to_numpy()[0]
        y0 = df_stations[df_stations['name'] == [row['Ends']][0]]['latitude'].to_numpy()[0]
        y1 = df_stations[df_stations['name'] == [row['Ends']][0]]['longitude'].to_numpy()[0]
        folium.PolyLine(locations=[(x0, x1),(y0, y1)], color='darkorange').add_to(foreign_lines)
foreign_lines


# In green: trains starting from outside, and coming to India. In orange: trains starting from India, and going outside.

# ## Demographics analysis

# ### Cities analysis

# In[28]:


df_cities_stations = pd.DataFrame()
cs_name = []
cs_nb_stations = []
cs_nb_start_trains = []
cs_nb_end_trains = []
cs_nb_trains = []
cs_population = []
cs_literacy = []
cs_literacy_gap = []
cs_graduate = []
cs_state = []
cs_latitude = []
cs_longitude = []


# In[29]:


stat_cities = set(df_stations['city'])
len(stat_cities)


# In[30]:


for sc in stat_cities:
    for C in df_cities['name_of_city']:
        if sc in C.lower().split(' ') or C.lower() in sc.split(' '):
            subset = df_stations[df_stations['city'] == sc]
            cs_name.append(C)
            cs_nb_stations.append(len(subset))
            cs_nb_start_trains.append(sum(subset['nb_starts']))
            cs_nb_end_trains.append(sum(subset['nb_ends']))
            cs_nb_trains.append(sum(subset['nb_trains']))
            cs_population.append(df_cities[df_cities['name_of_city'] == C]['population_total'].to_numpy()[0])
            cs_literacy.append(df_cities[df_cities['name_of_city'] == C]['effective_literacy_rate_total'].to_numpy()[0])
            cs_literacy_gap.append(df_cities[df_cities['name_of_city'] == C]['effective_literacy_rate_male'].to_numpy()[0] - df_cities[df_cities['name_of_city'] == C]['effective_literacy_rate_female'].to_numpy()[0])
            cs_graduate.append(df_cities[df_cities['name_of_city'] == C]['total_graduates'].to_numpy()[0])
            cs_state.append(df_cities[df_cities['name_of_city'] == C]['state_name'].to_numpy()[0])
            cs_latitude.append(df_cities[df_cities['name_of_city'] == C]['location'].to_numpy()[0].split(',')[0])
            cs_longitude.append(df_cities[df_cities['name_of_city'] == C]['location'].to_numpy()[0].split(',')[1])

df_cities_stations['name'] = cs_name
df_cities_stations['nb_stations'] = cs_nb_stations
df_cities_stations['nb_start_trains'] = cs_nb_start_trains
df_cities_stations['nb_end_trains'] = cs_nb_end_trains
df_cities_stations['nb_trains'] = cs_nb_trains
df_cities_stations['population'] = cs_population
df_cities_stations['literacy'] = cs_literacy
df_cities_stations['literacy_gap'] = cs_literacy_gap
df_cities_stations['graduate'] = cs_graduate
df_cities_stations['state'] = cs_state
df_cities_stations['latitude'] = cs_latitude
df_cities_stations['longitude'] = cs_longitude


# In[31]:


df_cities_stations.head(10)


# In[32]:


df_cities_stations.sort_values('nb_stations', ascending=False).head(20)


# Here, we can see that our algorithm did not treat efficiently the city sharing a same name (Mumbai and Delhi/New Delhi). Let's correct them manually.

# In[33]:


df_cities_stations = df_cities_stations.drop([82,83,148,149])
df_cities_stations.sort_values('nb_stations', ascending=False)


# In[34]:


df_stations[df_stations['city'] == 'new delhi']


# In[35]:


for idx, row in df_cities.iterrows():
    if 'new delhi' in row['name_of_city'].lower():
        df_cities_stations = df_cities_stations.append(
                                {'name':row['name_of_city'],
                                'nb_stations':1,
                                'nb_start_trains':120,
                                'nb_end_trains':123,
                                'nb_trains':243,
                                'population':df_cities[df_cities['name_of_city'] == row['name_of_city']]['population_total'].to_numpy()[0],
                                'literacy':df_cities[df_cities['name_of_city'] == row['name_of_city']]['effective_literacy_rate_total'].to_numpy()[0],
                                'literacy_gap':df_cities[df_cities['name_of_city'] == row['name_of_city']]['effective_literacy_rate_male'].to_numpy()[0] - df_cities[df_cities['name_of_city'] == row['name_of_city']]['effective_literacy_rate_female'].to_numpy()[0],
                                'graduate':df_cities[df_cities['name_of_city'] == row['name_of_city']]['total_graduates'].to_numpy()[0],
                                'state':df_cities[df_cities['name_of_city'] == row['name_of_city']]['state_name'].to_numpy()[0],
                                'latitude':df_cities[df_cities['name_of_city'] == row['name_of_city']]['location'].to_numpy()[0].split(',')[0],
                                'longitude':df_cities[df_cities['name_of_city'] == row['name_of_city']]['location'].to_numpy()[0].split(',')[1]
                                },
                                ignore_index=True)


# In[36]:


df_cities_stations.describe()


# In[37]:


df_cities_stations.hist(bins = 10 , figsize= (12,16))


# In[38]:


fig, axs = plt.subplots(1,2)
axs[0].scatter(df_cities_stations['nb_stations'],df_cities_stations['population'])
axs[0].set_xlabel('Number of stations')
axs[0].set_ylabel('Population')
axs[1].scatter(df_cities_stations['nb_trains'],df_cities_stations['population'])
axs[1].set_xlabel('Number of trains')
plt.show()


# In[39]:


many_stations = df_cities_stations[df_cities_stations['nb_stations'] >= 3]
many_stations


# In[40]:


df_cities_stations.sort_values('nb_trains', ascending=False).head(10)


# In[41]:


fig, ax = plt.subplots()
ax.scatter(df_cities_stations['nb_start_trains'],df_cities_stations['nb_end_trains'])
ax.set_xlabel('Number of starting trains')
ax.set_ylabel('Number of ending trains')
plt.show()


# In[42]:


fig, axs = plt.subplots(3,1,figsize=(12,12))
axs[0].scatter(df_cities_stations['literacy'],df_cities_stations['nb_trains'])
axs[0].set_ylabel('Number of trains')
axs[0].set_xlabel('Literacy')
axs[1].scatter(df_cities_stations['literacy_gap'],df_cities_stations['nb_trains'])
axs[1].set_ylabel('Number of trains')
axs[1].set_xlabel('Gender inequality against literacy')
axs[2].scatter(df_cities_stations['graduate']/df_cities_stations['population'],df_cities_stations['nb_trains'])
axs[2].set_ylabel('Number of trains')
axs[2].set_xlabel('Rate of graduated inhabitants')
plt.show()


# ### Stations and trains by state

# In[43]:


states = set(df_stations['state'])
stations_by_state = {}
for s in states:
    stations_by_state[s] = []
    for idx,row in df_stations.iterrows():
        if row['state'] == s:
            #stations_by_state[s].append(row['city'])
            c = row['city']
            for C in df_cities['name_of_city']:
                if c in C.lower() or C.lower() in c:
                    S = df_cities[df_cities['name_of_city'] == C]['state_name'].to_numpy()[0]
                    stations_by_state[s].append(S)


# In[44]:


stations_by_state


# In[45]:


stations_by_state.pop(8)
stations_by_state.pop('02')
stations_by_state.pop('04')
stations_by_state.pop('06')
stations_by_state.pop('07')
trad_table = {}
for sbs in stations_by_state:
    threshold = 2 * len(stations_by_state[sbs]) / 3
    sts = set(stations_by_state[sbs])
    aux = {}
    for s in sts:
        aux[s] = stations_by_state[sbs].count(s)
    trad_table[sbs] = ''
    for t in sts:
        if aux[t] >= threshold:
            trad_table[sbs] = t
            
trad_table


# Contd..
